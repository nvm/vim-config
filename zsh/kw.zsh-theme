if [ $UID -eq 0 ]; then NCOLOR="003"; else NCOLOR="222"; fi

PROMPT='$FX[bold]$FG[111]λ%{${reset_color}%} $FG[075]%c%{${reset_color}%} $(git_prompt_info)$FX[bold]$FG[$NCOLOR]%(!.».»)%{${reset_color}%} '
RPS1='$(vi_mode_prompt_info) %(?..$FG[111]%? ↵%{$reset_color%})'

ZSH_THEME_GIT_PROMPT_PREFIX="$FX[bold]$FG[070]±%{$reset_color%}$FG[075]"
ZSH_THEME_GIT_PROMPT_SUFFIX=" "
ZSH_THEME_GIT_PROMPT_CLEAN="%{$reset_color%}$FX[bold]$FG[070]✔%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$reset_color%}$FX[bold]$FG[214]⛌%{$reset_color%}"
#ZSH_THEME_GIT_PROMPT_DIRTY="%{$reset_color%}$FX[bold]$FG[214]⚡%{$reset_color%}"

MODE_INDICATOR="$FX[bold]$FG[075]<<<%{$reset_color%}"
