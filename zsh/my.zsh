make_cow_think() {
  COW_TYPE=$(\
    find /usr/share/cow*/cows -type f \
    | grep -v -e "telebears" -e "sodomized" -e "head-in" \
    | shuf -n 1\
  )

  WIDTH=$(expr $(stty size | cut -d" " -f2) - 4)

  cowthink -t -W ${WIDTH} -f ${COW_TYPE} "${1}"
}

inspire_cow() {
  make_cow_think "$(fortune -a)"
}

ctrlp() {
  </dev/tty vim -c CtrlP
}
zle -N ctrlp

bindkey "^p" ctrlp

alias ssh="TERM=xterm-256color ssh"
alias rgt="rg --iglob \!test"
alias cowtune="inspire_cow"

alias glog="git tree"
alias glist="git list"
alias vgrv="git diffall"
alias gsu="git submodule update --init --recursive"

alias wow="git status"
alias such="git"
alias very="git"

setopt flowcontrol

unsetopt beep

bindkey "^?" backward-delete-char
bindkey -M viins '\e[3~' vi-delete-char
bindkey -M vicmd '\e[3~' vi-delete-char

# zsh-navigation-tools
zle -N znt-kill-widget
bindkey "^Y" znt-kill-widget

zle -N znt-cd-widget
bindkey "^A" znt-cd-widget

# zsh-autosuggestions
bindkey '^ ' autosuggest-accept

eval $(thefuck --alias)

eval $(dircolors -b)
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

KEYTIMEOUT=1 # no delay for ESC in vi-mode

HISTSIZE=1000
SAVEHIST=1000

export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye >/dev/null
