#!/bin/bash

g++ -E -x c++ - -v < /dev/null 2>&1 \
  | awk ' /#include <...> search starts here:/ { show=1; next; } /End of search list./ { show=0; } { if (show) { print $0; } }' \
  | sed 's/^[[:space:]]*\(.*\)[[:space:]]*$/\1/'
