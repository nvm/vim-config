" Override YCM mappings for Haskell

nnoremap <buffer> <F5> :SyntasticCheck ghc_mod<CR>
nnoremap <buffer> <C-F5> :SyntasticReset<CR>

nnoremap <buffer> <F6> :GhcModInfo!<CR>
nnoremap <buffer> <C-F6> :GhcModExpand!<CR>
nnoremap <buffer> <F7> :GhcModType!<CR>
nnoremap <buffer> <C-F7> :GhcModTypeClear<CR>
nnoremap <buffer> <F9> :GhcModSplitFunCase!<CR>
nnoremap <buffer> <C-F9> :GhcModSigCodegen!<CR>
